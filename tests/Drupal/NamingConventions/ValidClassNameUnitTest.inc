<?php

class CorrectClassName {}
class CorrectClassWithAReallyLongName {}
class INCORRECT_CLASS_NAME {}
class INCORRECTCLASSNAME {}
class incorrectLowercaseClassName {}

interface CorrectInterfaceName {}
interface CorrectInterfaceWithAReallyLongName {}
interface INCORRECT_INTERFACE_NAME {}
interface INCORRECTINTERFACENAME {}
interface incorrectLowercaseInterfaceName {}

trait CorrectTraitName {}
trait CorrectTraitWithAReallyLongName {}
trait INCORRECT_TRAIT_NAME {}
trait INCORRECTTRAITNAME {}
trait incorrectLowercaseTraitName {}

enum CorrectEnumName {}
enum CorrectEnumWithAReallyLongName {}
enum INCORRECT_ENUM_NAME {}
enum INCORRECTENUMNAME {}
enum incorrectLowercaseEnumName {}
